#ifndef COMPASS_H
#define COMPASS_H

#include <QObject>
#include <QTimer>
#include <QtSensors/QCompass>

class SailfishCompass : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString direction READ direction NOTIFY directionChanged)
    Q_PROPERTY(qreal azimuth READ azimuth NOTIFY azimuthChanged)
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(QString azimuthString READ azimuthString NOTIFY azimuthStringChanged)
public:
    explicit SailfishCompass(QObject *parent = 0);

    QString direction();
    qreal azimuth();
    bool active();
    void setActive(bool);
    QString azimuthString();

signals:
    void directionChanged(const QString &direction);
    void azimuthChanged(qreal);
    void activeChanged(bool active);
    void azimuthStringChanged(const QString &azimuthString);
public slots:

private:
    QCompass *qCompass;
    QString degree2Letter(qreal degree);
    qreal currentDirection;
    bool running;
    QTimer *compassTimer;
private slots:
    void compassReadingChanged();

};

#endif // COMPASS_H

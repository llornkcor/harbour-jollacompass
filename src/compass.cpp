#include "compass.h"

#include <QtSensors/QCompass>
#include <QDebug>
#include <QTimer>

SailfishCompass::SailfishCompass(QObject *parent) :
    QObject(parent),
    currentDirection(0),
    running(false)
{
    qCompass = new QCompass(this);
    qCompass->connectToBackend();
    qCompass->setDataRate(100);
    qCompass->setAlwaysOn(false);
    compassTimer = new QTimer(this);
    compassTimer->setInterval(100);
    connect(compassTimer,SIGNAL(timeout()),this,SLOT(compassReadingChanged()));

   // connect(qCompass,SIGNAL(readingChanged()),this,SLOT(compassReadingChanged()));
}

QString SailfishCompass::direction()
{
    return degree2Letter(currentDirection);
}

QString SailfishCompass::degree2Letter(qreal degree)
{
    if (degree > 337.5 || degree < 22.5)
        return QStringLiteral("N");
    else if (degree > 22.5 && degree < 67.5)
        return QStringLiteral("NE");
    else if (degree > 67.5 && degree < 112.5)
        return QStringLiteral("E");
    else if (degree > 112.5 && degree < 157.5)
        return QStringLiteral("SE");
    else if (degree > 157.5 && degree < 202.5)
        return QStringLiteral("S");
    else if (degree > 202.5 && degree < 247.5)
        return QStringLiteral("SW");
    else if (degree > 247.5 && degree < 292.5)
        return QStringLiteral("W");
    else if (degree > 292.5 && degree < 337.5)
        return QStringLiteral("NW");
    return QStringLiteral("");
}

qreal SailfishCompass::azimuth()
{
    return currentDirection;
}

bool SailfishCompass::active()
{
    return running;
}

void SailfishCompass::setActive(bool b)
{
    running = b;
    if (running) {
        qCompass->start();
        compassTimer->start();
    } else {
        compassTimer->stop();
        qCompass->stop();
    }
    Q_EMIT activeChanged(running);
}

void SailfishCompass::compassReadingChanged()
{
    currentDirection = qCompass->reading()->azimuth();
    QString str = degree2Letter(currentDirection);
    Q_EMIT directionChanged(str);
    Q_EMIT azimuthChanged(currentDirection);

    azimuthString() = azimuthString();
    Q_EMIT azimuthStringChanged(str);
}

QString SailfishCompass::azimuthString()
{
    if (currentDirection < 10)
        return QStringLiteral("00") + QString::number(currentDirection);
    else if (currentDirection > 9 && currentDirection < 100)
        return QStringLiteral("0") + QString::number(currentDirection);
    return QString::number(currentDirection);
}

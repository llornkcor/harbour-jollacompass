# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-jollacompass
QT += sensors
CONFIG += sailfishapp

SOURCES += src/harbour-jollacompass.cpp \
          src/compass.cpp
HEADERS += src/compass.h

OTHER_FILES += qml/harbour-jollacompass.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/harbour-jollacompass.changes.in \
    rpm/harbour-jollacompass.spec \
    rpm/harbour-jollacompass.yaml \
    translations/*.ts \
    harbour-jollacompass.desktop

# to disable building translations every time, comment out the
# following CONFIG line
#CONFIG += sailfishapp_i18n
#TRANSLATIONS += translations/harbour-jollacompass-de.ts

RESOURCES += \
    compass.qrc


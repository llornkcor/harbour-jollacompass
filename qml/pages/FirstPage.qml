/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Lorn Potter <lorn.potter@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property int compassBearing: compassSensor.azimuth
//    property Item compassSensor: value
//    SailfishCompass {
//        id: compassSensor
//        active: Qt.application.active
////        dataRate: 4
//    }

    SilicaFlickable {
        id: screen
        anchors.fill: parent

//        PullDownMenu {
//            MenuItem {
//                text: "Show Page 2"
//                onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
//            }
//        }
        Image {
            id: headingTop
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            source: "qrc:///qml/images/graphic-compass-heading.png?" +Theme.highlightBackgroundColor
            horizontalAlignment: Text.AlignHCenter
            Text {
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeLarge * 2
                text: app.compassDirection
                anchors.horizontalCenter: parent.horizontalCenter
                y: Theme.paddingLarge * 2
                horizontalAlignment: Text.AlignHCenter

            }
        }

        Image {
            id: dial
            smooth: true
            source: "qrc:///qml/images/graphic-compass.png?" + Theme.highlightBackgroundColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            transform: Rotation {
                origin.x: dial.width / 2
                origin.y: dial.height / 2
                angle: -app.compassBearing
            }
//            transitions: Transition {
//                RotationAnimation {
//                    duration: 500
//                    direction: RotationAnimation.Shortest
//                    easing.type: Easing.InOutCubic
//                }
//            }
        }
        Row {
             anchors.centerIn: dial

            Text {
                id: degreeZeroslLabel
                opacity: 0.4
                color: Theme.highlightBackgroundColor
                font.pixelSize: Theme.fontSizeExtraLarge * 3
                anchors.verticalCenter: parent.verticalCenter
                text: preceedZero(app.compassBearing)
                function preceedZero(degree) {
                    if (degree < 10)
                        return "00"
                    else if (degree > 9 && degree < 100)
                        return "0"
                    else
                        return ""
                }
            }
            Text {
                id: degreeLabel
                color: Theme.highlightBackgroundColor
                font.pixelSize: Theme.fontSizeExtraLarge * 3
                text: app.compassBearing
            }
            Text {
                id: degreeSymbolLabel
                opacity: 0.4
                color: Theme.highlightBackgroundColor
                font.pixelSize: Theme.fontSizeExtraLarge * 3
                anchors.top: degreeLabel.top
                text: "\u00B0"
            }
        }
        Image {
            id: headingBottom

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            source: "qrc:///qml/images/graphic-compass-heading.png?" +Theme.highlightBackgroundColor
            opacity: 0.4
            transform: Rotation {
                angle: 180
                origin.x: headingBottom.width / 2
                origin.y: headingBottom.height / 2
            }
        }

    } //SilicaFlickable
}


